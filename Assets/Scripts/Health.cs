using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Health : MonoBehaviour
{
    //Esta clase puede ser usado en el personaje y en los enemigos.
    [SerializeField] private int health = 100;

    private int MAX_HEALTH = 100;

    public int healthBar;

    //public GameObject gameO;
   

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.D))
        {
           //Damage(10);
        }
        if (Input.GetKeyDown(KeyCode.H))
        {
           //Heal(10);
        }
    }

    public void SetHealth(int maxHealth, int health)
    {
        this.MAX_HEALTH = maxHealth;
        this.health = health;
    }

    public void Damage(int amount)
    {
        if (amount < 0)
        {
            throw new System.ArgumentOutOfRangeException("Cannot have negative damage");
        }
        
        this.health -= amount;

        if(health<=0)
        {
            Die();
        }
    }

    public void Heal(int amount)
    {
        if(amount < 0)
        {
            throw new System.ArgumentOutOfRangeException("Cannot have negative healing");
        }

        bool overMaxHealth = health + amount > MAX_HEALTH;

        if (overMaxHealth)
        {
            this.health = MAX_HEALTH;
        }
        else
        {
            this.health += amount;

        }
    }

    public float getHealth()
    {
        healthBar = this.health;

        return healthBar;
    }

    private void Die()
    {
        //Debug.Log($"Die {this.gameObject.tag}");
        if (this.gameObject.CompareTag("Player"))
        {
            //gameO.GetComponent<GameOver>().EndGame();
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
        else 
        {
            Destroy(gameObject);
        }
        
    }
}
