using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    //Se pueden crear distintos spawners con distintos enemigos
    [SerializeField] 
    private GameObject swarmerPrefab;
    [SerializeField]
    private GameObject redSwarmerPrefab;

    //Se crea un swarmer interval por cada tipo de enemigo que se haga
    [SerializeField]
    private float swarmerInterval = 2.5f; //Tiempo de spawn
    [SerializeField]
    private float redSwarmerInterval = 5.0f;
    // Start is called before the first frame update

    void Start()
    {
        //Se debe iniciar un coroutine por cada spawnEnemy con nuevo tipo de enemigo
        StartCoroutine(spawnEnemy(swarmerInterval, swarmerPrefab));
        StartCoroutine(spawnEnemy(redSwarmerInterval, redSwarmerPrefab));
    }

    private IEnumerator spawnEnemy(float interval, GameObject enemy)
    {
        //Crea un nuevo GameObject
        //En cierta posicion
        yield return new WaitForSeconds(interval);
        GameObject newEnemy = Instantiate(enemy, new Vector3(Random.Range(-5f, 5), Random.Range(-6f, 6f), 0), Quaternion.identity);
        //Con esto es infinito, puede cambiar a un numero de enemigos
        StartCoroutine(spawnEnemy(interval, enemy));
    }
}
