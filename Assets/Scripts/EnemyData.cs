using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName ="Data", menuName = "ScriptableObjects/Enemy", order =1)] //Permite que aparezca dentro del menu de assets

public class EnemyData : ScriptableObject
{
    //Valores que se pueden aplicar a distintos enemigos [pendiente de revisar]
    // Start is called before the first frame update
    public int hp;
    public int damage;
    public float speed;
}
