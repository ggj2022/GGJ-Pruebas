using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    private Image healthBar;
    public float CurrentHealth;
    private float MaxHealth=100f;

    private GameObject player;

    private void Start()
    {
        healthBar = GetComponent<Image>();
        player = GameObject.FindGameObjectWithTag("Player");
    }

    private void Update()
    {
        CurrentHealth = player.GetComponent<Health>().getHealth();
        healthBar.fillAmount = CurrentHealth / MaxHealth;
    }
}
