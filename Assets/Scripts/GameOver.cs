using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{
 
    public GameObject gameOver;

    public void GoBackMenu()
    {
        //Checar en build settings que los escenarios esten en orden, primero el menu y luego el juego
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 2);
     
    }
}
