using UnityEngine;

[RequireComponent(typeof(PlayerMovement))]
public class PlayerInput : MonoBehaviour
{
    [SerializeField] private PlayerMovement _playerMovement;
    float _horMove;
    float _verMove;
    bool _attack = false;

    void Awake() =>
        _playerMovement = GetComponent<PlayerMovement>();

    // Update is called once per frame
    void Update()
    {
        _horMove = Input.GetAxis("Horizontal");
        _verMove = Input.GetAxis("Vertical");
        _attack = Input.GetButtonDown("Fire1");
    }

    void FixedUpdate() =>
        _playerMovement.Move(_horMove, _verMove, _attack);

    private void OnTriggerStay2D(Collider2D collider)
    {
        if (_attack && (collider.tag == "Enemy" || collider.gameObject.name=="Enemy1(Clone)" || collider.gameObject.name=="Enemy1") )
        {
            //Debug.Log("Enemy reached");
        }
    }
}
