using UnityEngine;

public class LevelManager : MonoBehaviour
{
    [SerializeField] private GameObject _nekoPrefab;

    // Start is called before the first frame update
    void Start()
    {
        Instantiate(_nekoPrefab, new Vector3(0, 0, 0), Quaternion.identity);
    }

}
