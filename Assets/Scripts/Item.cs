using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
   
    [SerializeField]
    private int itemValue = 5;
    private bool collected;

    [SerializeField]
    private float speed = 3.5f;

    [SerializeField]
    private EnemyData data;

    private GameObject playerA;

    void Start()
    {
        playerA = GameObject.FindGameObjectWithTag("Player");
    }

    void Update()
    {
        Follow();
    }

    private void Follow()
    {
        transform.position = Vector2.MoveTowards(transform.position, playerA.transform.position, speed * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collected) return;
        
        if (collision.CompareTag("Player"))
        {
           
            if (collision.GetComponent<Health>() != null)
            {
                collision.GetComponent<Health>().Heal(itemValue);
                collected = true;
                Destroy(gameObject);
            }
        }
        else
        {
            return;
        }
    }

}
