using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSpawner : MonoBehaviour
{
    [SerializeField]
    private GameObject itemPrefab;

    [SerializeField]
    private float itemInterval = 5f; //Tiempo de spawn
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(spawnItem(itemInterval, itemPrefab));
    }

    private IEnumerator spawnItem(float interval, GameObject item)
    {
        //Crea un nuevo GameObject
        //En cierta posicion
        yield return new WaitForSeconds(interval);
        GameObject newEnemy = Instantiate(item, new Vector3(Random.Range(-5f, 5), Random.Range(-6f, 6f), 0), Quaternion.identity);
        //Con esto es infinito, puede cambiar a un numero de enemigos
        StartCoroutine(spawnItem(interval, item));
    }
}
