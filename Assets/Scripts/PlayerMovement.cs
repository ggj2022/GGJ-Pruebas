using System.Collections;
using UnityEngine;

[RequireComponent(typeof (Animator))]
[RequireComponent(typeof (Rigidbody2D))]
public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private float _horSpeed;
    [SerializeField] private float _verSpeed;
    [SerializeField] private bool _canMove = true;
    [SerializeField] private bool _facingRight = true;
    [SerializeField] private bool _isMoving = false;
    [SerializeField] private float _horMove;
    [SerializeField] private float _verMove;
    [SerializeField] private bool _punch;
    [SerializeField] private float _punchDelta = 0.5f;
    [SerializeField] private Animator _animator;
    [SerializeField] private Transform _punchArea;

    [Range(0, 1f)]
    [SerializeField] private float _movementSmooth = 0.5f;

    private Rigidbody2D _rigidbody;
    private Vector2 _velocity = Vector2.zero;
    private float _nextPunch = 0.5f;
    private float _myTime = 0f;
    //private IEnumerator _coroutine;

    void Awake()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();
    }

    public void Move(float horMove, float verMove, bool attack)
    {
        _myTime += Time.deltaTime;
        if(attack && _myTime > _nextPunch)
        {
            _nextPunch = _myTime + _punchDelta;
            _animator.SetTrigger("Punch");
            _nextPunch -= _myTime;
            _myTime = 0f;
            //StartCoroutine(AttackOff(1f));
            AudioManager.instance.Play("Attack_01");
            CheckGhostPunched();
        }

        if (_canMove)
        {
            _horMove = horMove;
            _verMove = verMove;

            
            if (Mathf.Approximately(0f, horMove)
            &&  Mathf.Approximately(0f, verMove))
            {
                _isMoving = false;
                _animator.SetBool("IsMoving", false);
            }
            else
            {
                _isMoving = true;

                _animator.SetBool("IsMoving", true);

                var _targetVelocity = new Vector2(
                    _horSpeed * horMove, _verSpeed * verMove);

                _rigidbody.velocity = Vector2.SmoothDamp(
                    _rigidbody.velocity,
                    _targetVelocity,
                    ref _velocity,
                    _movementSmooth);

                // Rotate if facing the opposite directiion.
                if (horMove > 0 && !_facingRight) Flip();
                else if (horMove < 0 && _facingRight) Flip();
            }   
        }
    }

    private void CheckGhostPunched()
    {
        Debug.Log("Checking punch...");
        var raycastHit = Physics2D.CircleCast(_punchArea.position, .3f, Vector2.up);
        if(raycastHit.collider != null)
        {
            Debug.Log($"hit with {raycastHit.collider.gameObject.tag}");
            if (raycastHit.collider.gameObject.CompareTag("Enemy"))
            {
                Debug.Log("Enemy punched!");
                //Destroy(raycastHit.collider.gameObject);
                raycastHit.collider.GetComponent<Health>().Damage(1000);
            }
        }
        
    }

    private IEnumerator AttackOff(float delay)
    {
        yield return new WaitForSeconds(delay);
        _animator.SetBool("Attack", false);

    }

    private void Flip()
    {
        //Debug.Log($"Flipping");
        _facingRight = !_facingRight;
        transform.Rotate(0, 180, 0);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawWireSphere(_punchArea.position, .3f);
    }
}
